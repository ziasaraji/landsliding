// todo : check the lazy loading
import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../views/Home.vue")
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "../views/About.vue")
    },
    {
      path: "/services",
      name: "services",
      component: () => import("../views/Services.vue")
    },
    {
      path: "/projects",
      name: "projects",
      component: () => import("../views/Projects.vue")
    },
    {
      path: "/panel",
      name: "panel",
      component: () => import("../views/Panel.vue"),
      children: [
        // { path: "", component: () => import("../views/Panel.vue") },
        {
          path: "admin_service", name: "admin_service",
          component: () => import("../components/panel/admin/Admin_Service.vue")
        },
        {
          path: "admin_request", name: "admin_request",
          component: () => import("../components/panel/admin/Admin_Request.vue")
        },
        {
          path: "admin_request_done", name: "admin_request_done",
          component: () => import("../components/panel/admin/Admin_Request_Done.vue")
        },
        {
          path: "admin_bill", name: "admin_bill",
          component: () => import("../components/panel/admin/Admin_Bill.vue")
        }
      ]
    },
    { path: "*", redirect: { name: "home" } }
  ]
});
// routes: [
//     { path: '/user/:id', component: User,
//       children: [
//         // UserHome will be rendered inside User's <router-view>
//         // when /user/:id is matched
//         { path: '', component: UserHome },

//         // UserProfile will be rendered inside User's <router-view>
//         // when /user/:id/profile is matched
//         { path: 'profile', component: UserProfile },

//         // UserPosts will be rendered inside User's <router-view>
//         // when /user/:id/posts is matched
//         { path: 'posts', component: UserPosts }
//       ]
//     }
// router.beforeEach((to, from, next) => {
//   // redirect to login page if not logged in and trying to access a restricted page
//   const publicPages = ["/login", "/register"];
//   const authRequired = !publicPages.includes(to.path);
//   const loggedIn = localStorage.getItem("user");

//   if (authRequired && !loggedIn) {
//     return next("/login");
//   }

//   next();
// });
