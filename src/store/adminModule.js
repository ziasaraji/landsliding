const user = JSON.parse(localStorage.getItem("user"));
import { adminService } from "../components/_api/admin";

// fixme : fix me :/
var state;
if (user) {
  if (user.role == "admin") {
    state = {
      loggedIn: true,
      user,
      services: [],
      serviceTableData: [],
      completeTableData: [],
      incompleteTableData: []
    };
  }
} else {
  state = {
    loggedIn: false,
    user: null
  };
}
const getters = {
  serviceTableData: state => {
    return state.serviceTableData;
  },
  completeTableData: state => {
    return state.completeTableData;
  },
  incompleteTableData: state => {
    return state.incompleteTableData;
  }
};

const actions = {
  async getApiServices({ commit }) {
    state.services = await adminService.showAllServices();
    // console.log(state.services);
    commit("creatServiceTableModel");
  },
  addNewService(service) {
    console.log(service);

    adminService.addNewService(service);
  },
  async getUserRequests({ commit }) {
    state.requests = await adminService.showUserRequests();
    // console.log(state.requests);
    commit("creatTableModel");
  }
};

const mutations = {
  tableState(state, item) {
    console.log(state);
    console.log(item);
    return "red";
  },
  addserviceTableData(state, item) {
    state.serviceTableData.push(item);
  },
  creatServiceTableModel(state) {
    state.serviceTableData = [];
    state.services.forEach(r => {
      var temp = {
        title: "",
        context: "",
        fullContext: "",
        price: 0
      };
      temp.title = r.title;
      temp.context = r.context.substr(0, 20) + "...";
      temp.fullContext = r.context;
      temp.price = r.price;
      state.serviceTableData.push(temp);
    });
  }
};

export const adminModule = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
