const user = JSON.parse(localStorage.getItem("user"));
import { accountService } from "../components/_api/account";
import { router } from "../router";

var state;
if (user) {
  if (user.role == "user") {
    state = {
      loggedIn: true,
      services: [],
      requests: [],
      tableData: [],
      user,
      links: [
        {
          title: "خانه",
          name: "home"
        },
        {
          title: "خدمات",
          name: "services"
        },
        {
          title: "پروژه‌ها",
          name: "projects"
        },
        { title: "درباره‌ی ما", name: "about" },
        {
          title: "پنل کاربری",
          name: "panel"
        }
      ],
      serviceTableData: [],
      completeTableData: [],
      incompleteTableData: []
    };
  } else if (user.role == "admin") {
    state = {
      loggedIn: true,
      services: [],
      user,
      links: [
        {
          title: "خانه",
          name: "home"
        },
        {
          title: "خدمات",
          name: "services"
        },
        {
          title: "پروژه‌ها",
          name: "projects"
        },
        { title: "درباره‌ی ما", name: "about" },
        {
          title: "پنل مدیر",
          name: "panel"
        }
      ],
      serviceTableData: [],
      completeTableData: [],
      incompleteTableData: []
    };
  }
} else {
  state = {
    loggedIn: false,
    user: null,
    links: [
      {
        title: "خانه",
        name: "home"
      },
      {
        title: "خدمات",
        name: "services"
      },
      {
        title: "پروژه‌ها",
        name: "projects"
      },
      { title: "درباره‌ی ما", name: "about" }
    ]
  };
}

const getters = {
  links: state => {
    return state.links;
  },
  loggedIn: state => {
    return state.loggedIn;
  },
  profile: state => {
    return state.user;
  },
  tableData: state => {
    return state.tableData;
  },
  getUser: state => {
    return state.user;
  },
  getServices: state => {
    return state.services;
  }
};

const actions = {
  async login({ commit }, user) {
    // localStorage.setItem("user", JSON.stringify(user));
    await accountService.login(user);
    commit("loginRequest");
    // accountService.login(username, password).then(
    //   user => {
    //     commit("loginSuccess", user);
    //     router.push("/");
    //   },
    //   error => {
    //     commit("loginFailure", error);
    //     dispatch("alert/error", error, { root: true });
    //   }
    // );
  },

  logout({ commit }) {
    commit("logout");
    // accountService.logout();
  },

  async register({ commit }, user) {
    // commit("registerRequest", user);
    await accountService.register(user);
    commit("loginRequest");
    // accountService.register(user).then(
    //   user => {
    //     commit("registerSuccess", user);
    //     router.push("/login");
    //     setTimeout(() => {
    //       // display success message after route change completes
    //       dispatch("alert/success", "Registration successful", { root: true });
    //     });
    //   },
    //   error => {
    //     commit("registerFailure", error);
    //     dispatch("alert/error", error, { root: true });
    //   }
    // );
  }
};

const mutations = {
  loginRequest(state) {
    const user = JSON.parse(localStorage.getItem("user"));
    state.user = user;
    if (user) {
      if (user.role == "user") {
        state.loggedIn = true;
        state.links.push({
          title: "پنل کاربری",
          name: "panel"
        });
        router.push({ name: "panel" });
      } else if (user.role == "admin") {
        state.loggedIn = true;
        state.links.push({
          title: "پنل مدیر",
          name: "panel"
        });
        router.push({ name: "panel" });
      }
    }
  },

  logout(state) {
    state.loggedIn = false;
    state.user = null;
    if (user) {
      localStorage.removeItem("user");
    }
    state.links.pop();
    router.push({ name: "home" });
  },

  // registerRequest(state) {
  //   state.loggedIn = true;
  //   state.user = user;
  //   state.links.push({
  //     title: "پنل کاربری",
  //     name: "panel"
  //   });
  // },
  // registerSuccess(state) {
  //   state.status = {};
  // },
  // registerFailure(state) {
  //   state.status = {};
  // },
  // initializeTable(state) {
  //   // state.tableData = [
  //   //   {
  //   //     location: "(142:17)",
  //   //     address: "خیابان بابل کوچه اول",
  //   //     area: 60,
  //   //     service: [
  //   //       { name: "سرویس اول", price: 120, text: "کار شما انجام شد" },
  //   //       { name: "سرویس دوم", price: 750 },
  //   //       { name: "سرویس سوم", price: 520 }
  //   //     ],
  //   //     state: [1, 0, -1],
  //   //     date: "1398/12/04"
  //   //   },
  //   //   {
  //   //     location: "(12:145)",
  //   //     address: "خیابان بابل کوچه دوم",
  //   //     area: 90,
  //   //     service: [{ name: "سرویس سوم", price: 520 }],
  //   //     state: [0, 0, 1],
  //   //     date: "1398/12/05"
  //   //   },
  //   //   {
  //   //     location: "(14:171)",
  //   //     address: "خیابان بابل کوچه سوم",
  //   //     area: 160,
  //   //     service: [{ name: "سرویس دوم", price: 140 }],
  //   //     state: [1],
  //   //     date: "1398/12/06"
  //   //   }
  //   // ];
  // },
  tableState(state, item) {
    console.log(state);
    console.log(item);
    return "red";
  },
  addTableData(state, item) {
    state.tableData.push(item);
  },
  creatTableModel(state) {
    state.tableData = [];
    var temp = {
      date: "",
      location: "",
      area: 0,
      services: 0,
      status: "",
      details: {}
    };
    state.requests.forEach(r => {
      temp.date = r.date;
      temp.location =
        "(" + r.location.coordinates[0] + "," + r.location.coordinates[1] + ")";
      temp.area = r.area;
      temp.services = r.demand.length;
      if (r.status == "initial") {
        temp.status = "بدون پاسخ";
      } else if (r.status == "responed") {
        temp.status = "پاسخ داده شد";
      } else if (r.status == "done") {
        temp.status = "تکمیل شده";
      }
      temp.details = r;
      state.tableData.push(temp);
    });
  }
};

export const accountModule = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
