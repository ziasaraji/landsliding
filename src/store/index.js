// import Vue from 'vue'
// import Vuex from 'vuex'

// Vue.use(Vuex)

// export default new Vuex.Store({
//   state: {
//   },
//   mutations: {
//   },
//   actions: {
//   },
//   modules: {
//   }
// })

import Vue from "vue";
import Vuex from "vuex";

import { accountModule } from "./accountModule";
import { userModule } from "./userModule";
import { adminModule } from "./adminModule";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drawer: false,
    login: false
  },
  getters: {
    loginForm: state => {
      return state.login;
    }
  },
  mutations: {
    showLoginDialog: state => {
      state.login = !state.login;
    }
  },
  modules: {
    accountModule,
    userModule,
    adminModule
  }
});
