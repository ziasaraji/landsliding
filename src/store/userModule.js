const user = JSON.parse(localStorage.getItem("user"));
import { userService } from "../components/_api/user";

var state;
if (user) {
  if (user.role == "user") {
    state = {
      loggedIn: true,
      services: [],
      requests: [],
      tableData: [],
      user,
    };
  }
} else {
  state = {
    loggedIn: false,
    user: null,
  };
}
const getters = {
  tableData: state => {
    return state.tableData;
  },
  getUser: state => {
    return state.user;
  },
  getServices: state => {
    return state.services;
  }
};

const actions = {
  async getApiServices() {
    state.services = await userService.showAllServices();
    // console.log(state.services);
  },
  async getUserRequests({ commit }) {
    state.requests = await userService.showUserRequests();
    // console.log(state.requests);
    commit("creatTableModel");
  }
};

const mutations = {
  // fixme : fix table column state
  tableState(state, item) {
    console.log(state);
    console.log(item);
    return "red";
  },
  // fixme : send data
  addTableData(state, item) {
    state.tableData.push(item);
  },
  creatTableModel(state) {
    state.tableData = [];
    var temp = {
      date: "",
      location: "",
      area: 0,
      services: 0,
      status: "",
      details: {}
    };
    state.requests.forEach(r => {
      temp.date = r.date;
      temp.location =
        "(" + r.location.coordinates[0] + "," + r.location.coordinates[1] + ")";
      temp.area = r.area;
      temp.services = r.demand.length;
      if (r.status == "initial") {
        temp.status = "بدون پاسخ";
      } else if (r.status == "responed") {
        temp.status = "پاسخ داده شد";
      } else if (r.status == "done") {
        temp.status = "تکمیل شده";
      }
      temp.details = r;
      state.tableData.push(temp);
    });
  }
};

export const userModule = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
