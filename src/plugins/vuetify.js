import Vue from "vue";
import Vuetify from "vuetify/lib";
import fa from "vuetify/es5/locale/fa";
import "../stylus/main.styl";

Vue.use(Vuetify);

export default new Vuetify({
  rtl: true,
  theme: {
    options: {
      customProperties: true
    },
    themes: {
      light: {
        primary: {
          base: "#ff8040",
          darken1: "#ff5500",
          lighten1: "#ffaa80"
        },
        accent: {
          base: "#2196F3",
          lighten1: "#B6DDFC"
        },
        info: '#2196F3',
        error: "#FF5252",
        warning: "#FFC107",
        success: "#4CAF50",
        neutral: {
          base: "#334D6E",
          lighten1: "#C2CFE0",
          lighten2: "#EDF1F2"
        }
      }
    }
  },
  lang: {
    locales: { fa },
    current: "fa"
  },
  breakpoint: {
    // thresholds: {
    //   xs: 340,
    //   sm: 600,
    //   md: 768,
    //   lg: 1440,
    //   xl: 3200
    // },
    // scrollBarWidth: 2
  }
});
