import axios from "axios";
export const userService = {
  showAllServices,
  showUserRequests
};

async function showAllServices() {
  var s = [];
  await axios
    .get("http://api.landsliding.ir/api/service/showall")
    .then(services => {
      if (services.data.code == 0) {
        alert("لطفا صفحه را دوباره بارگذاری کنید");
      } else if (services.data.code == 1) {
        s = services.data.services;
      }
    })
    .catch(err => {
      console.log("err", err);
    });
  return s;
}

async function showUserRequests() {
  var s = [];
  const user = JSON.parse(localStorage.getItem("user"));
  await axios
    .post("http://api.landsliding.ir/api/user/showUserRequests",{
      userID:user._id
    })
    .then(requests => {
      if (requests.data.code == 0) {
        alert("لطفا صفحه را دوباره بارگذاری کنید");
      } else if (requests.data.code == 1) {
        s = requests.data.requests;
      }
    })
    .catch(err => {
      console.log("err", err);
    });
  return s;
}
