import axios from "axios";
export const accountService = {
    login,
    logout,
    register,
};

async function login(user) {
    // const requestOptions = {
    //   // method: "POST",
    //   headers: { "Content-Type": "application/json" },
    //   body: JSON.stringify({ nationalcode, password })
    // };
    var body = {
        nationalcode: user.nationalcode,
        password: user.password
    };

    try {
        await axios
            .post("http://api.landsliding.ir/api/user/login", body)
            .then(user => {
                if (user) {
                    localStorage.setItem("user", JSON.stringify(user.data.user));
                }
                return user;
            })
            .catch(err => {
                console.log("err", err);
            });
    } catch (error) {
        console.log(error);
    }

    //   return fetch(`api.landsliding.ir/users/authenticate`, requestOptions)
    //     .then(handleResponse)
    //     .then(user => {
    //       if (user.token) {
    //         localStorage.setItem("user", JSON.stringify(user));
    //       }
    //       return user;
    //     });
}

// fixme : send to api
function logout() {
    localStorage.removeItem("user");
}

async function register(user) {
    var body = {
        nationalcode: user.nationalcode,
        email: user.email,
        phoneNumber: user.phoneNumber,
        name: user.name,
        password: user.password
    };

    try {
        await axios
            .post("http://api.landsliding.ir/api/user/signup", body)
            .then(user => {
                if (user) {
                    localStorage.setItem("user", JSON.stringify(user.data.user));
                }
                return user;
            })
            .catch(err => {
                console.log("err", err);
            });
    } catch (error) {
        console.log(error);
    }
}